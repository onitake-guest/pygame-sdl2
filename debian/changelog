pygame-sdl2 (7.4.2-1python2) UNRELEASED; urgency=medium

  * Non-maintainer upload.
  * Repackage Python 2 version for renpy

 -- Gregor Riepl <onitake@gmail.com>  Thu, 22 Jul 2021 23:24:22 +0200

pygame-sdl2 (7.4.2-1) unstable; urgency=medium

  * New upstream version 7.4.2.

 -- Markus Koschany <apo@debian.org>  Sun, 07 Feb 2021 01:54:54 +0100

pygame-sdl2 (7.4.1-1) unstable; urgency=medium

  * New upstream version 7.4.1.

 -- Markus Koschany <apo@debian.org>  Sun, 31 Jan 2021 00:06:38 +0100

pygame-sdl2 (7.4.0-2) unstable; urgency=medium

  * Remove gen3 directory in clean target again. (Closes: #980658)

 -- Markus Koschany <apo@debian.org>  Thu, 21 Jan 2021 18:09:07 +0100

pygame-sdl2 (7.4.0-1) unstable; urgency=medium

  * New upstream version 7.4.0.
  * Declare compliance with Debian Policy 4.5.1.

 -- Markus Koschany <apo@debian.org>  Fri, 08 Jan 2021 19:45:00 +0100

pygame-sdl2 (7.3.5-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream version 7.3.5.
  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.0.

 -- Markus Koschany <apo@debian.org>  Fri, 31 Jul 2020 13:23:55 +0200

pygame-sdl2 (7.3.3-1) experimental; urgency=medium

  * New upstream version 7.3.3.
  * Declare compliance with Debian Policy 4.4.1.
  * Switch to python3 and create the new binary package python3-pygame-sdl2.
    (Closes: #937442)

 -- Markus Koschany <apo@debian.org>  Thu, 03 Oct 2019 22:08:15 +0200

pygame-sdl2 (7.3.2-1) unstable; urgency=medium

  * New upstream version 7.3.2.
  * Switch to debhelper-compat = 12.
  * Declare compliance with Debian Policy 4.4.0.
  * Use canonical VCS URI.

 -- Markus Koschany <apo@debian.org>  Fri, 19 Jul 2019 02:42:22 +0200

pygame-sdl2 (7.1.1-1) unstable; urgency=medium

  * New upstream version 7.1.1.

 -- Markus Koschany <apo@debian.org>  Mon, 15 Oct 2018 12:04:34 +0200

pygame-sdl2 (7.1.0-1) unstable; urgency=medium

  * New upstream version 7.1.0.
  * Declare compliance with Debian Policy 4.2.1.

 -- Markus Koschany <apo@debian.org>  Thu, 13 Sep 2018 14:30:04 +0200

pygame-sdl2 (6.99.14.3-1) unstable; urgency=medium

  * New upstream version 6.99.14.3.
  * Drop sdl2-mixer.patch. Fixed upstream.

 -- Markus Koschany <apo@debian.org>  Sun, 08 Apr 2018 21:33:53 +0200

pygame-sdl2 (6.99.14.1-1) unstable; urgency=medium

  * New upstream version 6.99.14.1.

 -- Markus Koschany <apo@debian.org>  Thu, 08 Feb 2018 18:12:45 +0100

pygame-sdl2 (6.99.14-1) unstable; urgency=medium

  * New upstream version 6.99.14.
  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.1.3.

 -- Markus Koschany <apo@debian.org>  Mon, 15 Jan 2018 18:13:26 +0100

pygame-sdl2 (6.99.13-1) unstable; urgency=medium

  * New upstream version 6.99.13.
  * Declare compliance with Debian Policy 4.1.1.
  * Remove gen3 directory in dh_clean override.
  * Add sdl2-mixer.patch and fix FTBFS with version 2.0.2 of libsdl2-mixer.

 -- Markus Koschany <apo@debian.org>  Sat, 11 Nov 2017 22:40:44 +0100

pygame-sdl2 (6.99.12.4-1) unstable; urgency=medium

  * New upstream version 6.99.12.4.
  * Use https for Format field.
  * export DEB_BUILD_MAINT_OPTIONS = hardening=+all.

 -- Markus Koschany <apo@debian.org>  Mon, 19 Jun 2017 01:40:00 +0200

pygame-sdl2 (6.99.12.2-1) unstable; urgency=medium

  * New upstream version 6.99.12.2.

 -- Markus Koschany <apo@debian.org>  Wed, 04 Jan 2017 22:28:48 +0100

pygame-sdl2 (6.99.12.1-1) unstable; urgency=medium

  * New upstream version 6.99.12.1.

 -- Markus Koschany <apo@debian.org>  Sun, 25 Dec 2016 03:47:20 +0100

pygame-sdl2 (6.99.12-1) unstable; urgency=medium

  * New upstream version 6.99.12.
  * Switch to compat level 10.

 -- Markus Koschany <apo@debian.org>  Wed, 21 Dec 2016 11:59:28 +0100

pygame-sdl2 (6.99.11-1) unstable; urgency=medium

  * New upstream version 6.99.11.
  * Declare compliance with Debian Policy 3.9.8.

 -- Markus Koschany <apo@debian.org>  Wed, 07 Sep 2016 00:32:32 +0200

pygame-sdl2 (6.99.10-1) unstable; urgency=medium

  * Imported Upstream version 6.99.10.

 -- Markus Koschany <apo@debian.org>  Wed, 30 Mar 2016 16:07:14 +0200

pygame-sdl2 (6.99.9-1) unstable; urgency=medium

  * Imported Upstream version 6.99.9.
  * Build with all default hardening build flags again. (Closes: #812695)
  * Update debian/watch file.
  * Add get-orig-source target.
  * Drop write_png.c.patch. Applied upstream.
  * Depend on fonts-dejavu-core and symlink the font.
  * Declare compliance with Debian Policy 3.9.7.

 -- Markus Koschany <apo@debian.org>  Sun, 13 Mar 2016 01:36:59 +0100

pygame-sdl2 (6.99.8-3) unstable; urgency=medium

  * Add write_png.c.patch and fix FTBFS on Big-endian architectures due to
    missing semicolon.

 -- Markus Koschany <apo@debian.org>  Tue, 26 Jan 2016 13:15:45 +0100

pygame-sdl2 (6.99.8-2) unstable; urgency=medium

  * Vcs-Git: Switch to https.
  * Build without -format hardening flag for now until an issue with SDL2
    2.0.4 is resolved. This prevents the FTBFS. (Closes: #812695)

 -- Markus Koschany <apo@debian.org>  Tue, 26 Jan 2016 02:09:47 +0100

pygame-sdl2 (6.99.8-1) unstable; urgency=medium

  * Initial release. (Closes: #811570)

 -- Markus Koschany <apo@debian.org>  Thu, 21 Jan 2016 11:46:40 +0100
